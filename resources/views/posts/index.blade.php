@extends('layouts.app')

@section('content')
    @if(count($posts) > 0)
        @foreach($posts as $post)
        @if($post->isActive==true)
            <div class="card text-center container w-25 mb-3">
                <div class="card-body">
                    <h4 class="card-title mb-3"><a href="/posts/{{$post->id}}">{{$post->title}}</a></h4>
                    <h6 class="card-text mb-3">Author: {{$post->user->name}}</h6>
                    <p class="card-subtitle mb-3 text-muted">Created at: {{$post->created_at}}</p>
                    {{-- check if there is an authenticated user to prevent our web app from throwing an error when no user is logged in --}}
                </div>
                @if(Auth::user())
                    {{-- if the authenticated user is the author of this blog post --}}
                    @if(Auth::user()->id == $post->user_id)
                        {{-- show an edit post button and a delete post button --}}
                        <div class="card-footer">
                            <form method="POST" action="/posts/{{$post->id}}">
                                @method('DELETE')
                                @csrf
                                <a href="/posts/{{$post->id}}/edit" class="btn btn-primary">Edit post</a>

                                <!-- <a href="/posts/{{$post->id}}" class="btn btn-danger">Archive post</a>
 -->
                                <button type="submit" class="btn btn-danger">Archive Post</button>



                            </form>
                        </div>
                        @endif
                    @endif
                @endif
            </div>
        @endforeach
    @else
        <div class="container pt-5 w-50">
            <h2>There are no posts to show</h2>
            <a href="/posts/create" class="btn btn-primary">Create post</a>
        </div>
    @endif
@endsection
