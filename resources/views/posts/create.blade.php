@extends('layouts.app')

@section('content')

  <div class="container p-5 w-50">
    <form method="POST" action="/posts">
        @csrf
      <div class="form-group">
        <label for="title">Title:</label>
        <input type="text" class="form-control" id="title" name="title">
      </div>
      <div class="form-group">
        <label for="content ">Content:</label>
        <textarea class="form-control" id="content" name="content" rows="3"></textarea>
      </div>
      <div class="mt-2">
      	<button type="submit" class="mt-3 btn btn-success">Create Post</button>
  	  </div>
    </form>
  </div>
@endsection
