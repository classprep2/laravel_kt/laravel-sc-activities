<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PostController::class, 'welcome']);

//define a route wherein a view to create a post will be returned to the user
Route::get('/posts/create', [PostController::class, 'create']);
//define a route wherein form data will be sent via POST method to the /posts URI endpoint
Route::post('/posts', [PostController::class, 'store']);
//define a route that will return a view containing all posts
Route::get('/posts', [PostController::class, 'index']);
//define a route that will return a view containing only the authenticated user's posts
Route::get('/myPosts', [PostController::class, 'myPosts']);
//define a route wherein a view showing a specific post with matching URL parameter ID will be returned to the user
Route::get('/posts/{id}', [PostController::class, 'show']);
//define a route wherein a view to edit a post with matching URL parameter ID will be returned to the user
Route::get('/posts/{id}/edit', [PostController::class, 'edit']);
//define a route that will overwrite an existing post with matching URL parameter ID via PUT method
Route::put('/posts/{id}', [PostController::class, 'update']);



//define a route that will allow users to like posts
Route::put('/posts/{id}/like', [PostController::class, 'like']);
//define a route for posting a comment
Route::post('/posts/{id}/comment', [PostController::class, 'comment']); 


// s04 activity
//define a route that will delete a post of the matching URL parameter ID
Route::delete('/posts/{id}', [PostController::class, 'archive']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
