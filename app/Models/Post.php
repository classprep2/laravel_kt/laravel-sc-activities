<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    public function user()   
    {
        return $this->belongsTo('App\Models\User');
    }

    // s05
    public function likes()
    {
        return $this->hasMany('App\Models\PostLike');
    }

    // s05 Activity
    public function comments()
    {
        return $this->hasMany('App\Models\PostComment');
    }




}
